import 'package:catbreedapp/ui/pages/splash_screem_page.dart';
import 'package:flutter/material.dart';

import 'ui/molecules/cat_breed_card_molecule.dart';
import 'ui/pages/cats_breed_page.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<bool>(
        future: Future.delayed(const Duration(seconds: 2), () => true),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return const MaterialApp(
                debugShowCheckedModeBanner: false,
                home: SafeArea(child: SplashScreemPage()));
          }
          return const MaterialApp(
              debugShowCheckedModeBanner: false,
              title: 'CatsBreed',
              home: SafeArea(child: FirstPage()));
        });
  }
}
