import 'package:catbreedapp/domain/models/catbreed_model.dart';
import 'package:catbreedapp/domain/models/gateway/catbreed_gateway.dart';

class CatBreedUseCase {
  // TODO: Hacer mas clara la implementación.
  final CatBreedsGateway _catBreedGateway;
  CatBreedUseCase(this._catBreedGateway);
  Future<List<CatBreed>> getAllCatBreeds() {
    return _catBreedGateway.getCatBreed();
  }
}
