import '../catbreed_model.dart';

abstract class CatBreedsGateway {
  Future<List<CatBreed>> getCatBreed();
}
