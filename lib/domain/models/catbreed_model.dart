import 'dart:convert';

CatBreed catBreedFromMap(String str) => CatBreed.fromMap(json.decode(str));

String catBreedToMap(CatBreed data) => json.encode(data.toMap());

class CatBreed {
  CatBreed({required this.countryCode,required this.altNames,
    required this.id,
    required this.name,
    required this.temperament,
    required this.origin,
    required this.description,
    required this.lifeSpan,
    required this.adaptability,
    required this.healthIssues,
    required this.intelligence,
    this.wikipediaUrl,
    required this.image,
  });


  final String id;
  final String name;
  final String temperament;
  final String origin;
  final String countryCode;
  final String description;
  final String lifeSpan;
  final String? altNames;
  final int adaptability;
  final int healthIssues;
  final int intelligence;
  final String? wikipediaUrl;
  final ImageCat image;

  factory CatBreed.fromMap(Map<String, dynamic> json) => CatBreed(
        id: json["id"],
        name: json["name"],
        temperament: json["temperament"],
        origin: json["origin"],
        countryCode: json["country_code"],
        description: json["description"],
        lifeSpan: json["life_span"],
        altNames: json["alt_names"],
        adaptability: json["adaptability"],
        healthIssues: json["health_issues"],
        intelligence: json["intelligence"],
        wikipediaUrl: json["wikipedia_url"],
        image: ImageCat.fromMap(json["image"]),
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "name": name,
        "temperament": temperament,
        "origin": origin,
        "country_code": countryCode,
        "description": description,
        "life_span": lifeSpan,
        "alt_names": altNames,
        "adaptability": adaptability,
        "health_issues": healthIssues,
        "intelligence": intelligence,
        "wikipedia_url": wikipediaUrl,
        "image": image.toMap(),
      };
}

class ImageCat {
  ImageCat({
    required this.id,
    required this.width,
    required this.height,
    required this.url,
  });

  final String? id;
  final int? width;
  final int? height;
  final String? url;

  factory ImageCat.fromMap(Map<String, dynamic>? json) => ImageCat(
        id: json?["id"],
        width: json?["width"],
        height: json?["height"],
        url: json?["url"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "width": width,
        "height": height,
        "url": url,
      };
}

class Weight {
  Weight({
    required this.imperial,
    required this.metric,
  });

  final String imperial;
  final String metric;

  factory Weight.fromMap(Map<String, dynamic> json) => Weight(
        imperial: json["imperial"],
        metric: json["metric"],
      );

  Map<String, dynamic> toMap() => {
        "imperial": imperial,
        "metric": metric,
      };
}
