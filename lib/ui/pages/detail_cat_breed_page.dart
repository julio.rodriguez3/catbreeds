import 'package:catbreedapp/domain/models/catbreed_model.dart';
import 'package:catbreedapp/ui/organisms/details_items_organism.dart';
import 'package:catbreedapp/ui/templates/detail_cat_breed_template.dart';

import '../atoms/image_netwok_atom.dart';

const double _sizeText = 15.0;

class DetailCatBreedPage extends StatelessWidget {
  final CatBreed catBreed;
  const DetailCatBreedPage({Key? key, required this.catBreed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DetailCatBreedTemplate(
        header: catBreed.name,
        body: ImageNetworkAtom(imagePatch: catBreed.image.url, height: 200),
        footer: Column(
          children: [
            Text(
              catBreed.description,
              textAlign: TextAlign.justify,
              style: const TextStyle(fontSize: _sizeText),
            ),
            const SizedBox(height: 20),
            DetailsItemOrganism(
              catData: catBreed,
              sizeText: _sizeText,
            )
          ],
        ));
  }
}
