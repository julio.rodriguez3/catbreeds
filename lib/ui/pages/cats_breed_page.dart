import 'package:catbreedapp/ui/organisms/list_cat_breed_card.dart';
import 'package:catbreedapp/ui/templates/list_cats_breed_template.dart';
import 'package:flutter/material.dart';

import '../../config/catbreeds_bloc.dart';
import '../../domain/models/catbreed_model.dart';
import '../molecules/breed_button_card_molecule.dart';
import '../molecules/search_cat_breed_molecule.dart';

class FirstPage extends StatelessWidget {
  const FirstPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListCatBreedTemplate(
        header: SearchCatBreedMolecule(
          funtion: catbreedsBloc.filterCatsBreedForName,
          catsBreeds: catbreedsBloc.catsBreed,
        ),
        body: StreamBuilder<List<CatBreed>>(
            stream: catbreedsBloc.streamData.stream,
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return const Center(
                    //        child: CircularProgressIndicator(),
                    );
              }
              List<Widget> catsBreed=List<Widget>.generate(snapshot.data!.length, (index) => BreedButtonCardMolecule(
                catBreed: snapshot.data![index],
              ));
              return Expanded(
                child: ListCatBreedCardOrganism(catsBreed: catsBreed),
              );
            }));
  }
}
