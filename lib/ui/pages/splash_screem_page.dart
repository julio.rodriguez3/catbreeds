import 'package:catbreedapp/ui/templates/splash_screem_template.dart';
import 'package:flutter/material.dart';

class SplashScreemPage extends StatelessWidget {
  const SplashScreemPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SplashScreemTemplate(
        header: const Text("CatBreed",
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
        body: Image.asset("assets/gato.gif"));
  }
}
