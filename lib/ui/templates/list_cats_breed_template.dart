import 'package:flutter/material.dart';

class ListCatBreedTemplate extends StatelessWidget {
  final Widget header;
  final Widget body;
  const ListCatBreedTemplate(
      {Key? key, required this.header, required this.body})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('CatsBreed'),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          // mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8),
              child: header,
            ),
            body,
          ],
        ),
      ),
    );
  }
}
