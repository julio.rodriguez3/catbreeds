import 'package:flutter/material.dart';

class SplashScreemTemplate extends StatelessWidget {
  final Widget header;
  final Widget body;
  const SplashScreemTemplate(
      {Key? key, required this.header, required this.body})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [header, body],
        ),
      ),
    );
  }
}
