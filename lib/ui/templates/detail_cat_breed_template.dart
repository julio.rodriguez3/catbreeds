import 'package:flutter/material.dart';

class DetailCatBreedTemplate extends StatelessWidget {
  final String header;
  final Widget body;
  final Widget footer;
  const DetailCatBreedTemplate(
      {Key? key,
      required this.header,
      required this.body,
      required this.footer})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(header),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Column(
          children: [
            body,
            Expanded(child: SingleChildScrollView(child: footer))
          ],
        ),
      ),
    );
  }
}
