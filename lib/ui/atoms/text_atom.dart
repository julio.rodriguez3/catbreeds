import 'package:flutter/material.dart';
export  'package:flutter/material.dart';

class TextAtom extends StatelessWidget {
  final String text;
  final TextStyle? textStyle;
  final TextAlign? textAlign;
  final double widthTextBox;
  const TextAtom({Key? key,required this.text , this.textStyle,this.textAlign, this.widthTextBox=80}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(

        width: widthTextBox,
        child: Text(text,style: textStyle,textAlign: textAlign,));
  }
}
