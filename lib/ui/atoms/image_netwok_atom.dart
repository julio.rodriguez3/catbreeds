import 'package:flutter/material.dart';
export 'package:flutter/material.dart';

class ImageNetworkAtom extends StatelessWidget {
  final String? imagePatch;
  final double? height;
  final double? width;
  final EdgeInsets padding;
  const ImageNetworkAtom(
      {Key? key,
      required this.imagePatch,
      this.height,
      this.width,
      this.padding = const EdgeInsets.all(8)})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return imagePatch == null
        ? const SizedBox()
        : Padding(
            padding: padding,
            child: Image.network(
              imagePatch!,
              width: width,
              height: height,
            ),
          );
  }
}
