import 'package:catbreedapp/ui/molecules/cat_breed_card_molecule.dart';
import 'package:flutter/gestures.dart';

class TextSpanAtom extends TextSpan {
  const TextSpanAtom(
      {List<InlineSpan>? children,
      String? text,
      TextStyle? style,
      GestureRecognizer? recognizer})
      : super(
            children: children,
            text: text,
            style: style,
            recognizer: recognizer);
}
