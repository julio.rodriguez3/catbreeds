import 'package:flutter/material.dart';

extension ReponsiveValue on MediaQueryData {
  bool get isMobil {
    return size.width < 576;
  }
}