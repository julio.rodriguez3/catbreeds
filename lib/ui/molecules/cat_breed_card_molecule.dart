import '../../domain/models/catbreed_model.dart';
import '../atoms/image_netwok_atom.dart';
import '../atoms/text_atom.dart';

export 'package:flutter/material.dart';

class CatBreedCardMolecule extends StatelessWidget {
  final CatBreed catBreed;
  const CatBreedCardMolecule({Key? key, required this.catBreed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TextAtom(
            text: catBreed.name,
            textAlign: TextAlign.center,
            textStyle: const TextStyle(fontWeight: FontWeight.bold),
            widthTextBox: 200),
        ImageNetworkAtom(
          imagePatch: catBreed.image.url,
          height: 200,
          width: 200,
        ),
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            TextAtom(
              text: catBreed.origin,
              textStyle: const TextStyle(fontWeight: FontWeight.bold),
            ),
            TextAtom(
              text: "${catBreed.lifeSpan} years",
              widthTextBox: 90,
              textStyle: const TextStyle(fontWeight: FontWeight.bold),
            ),
          ],
        )
      ],
    );
  }
}
