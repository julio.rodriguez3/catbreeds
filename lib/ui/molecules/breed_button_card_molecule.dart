import 'package:catbreedapp/domain/models/catbreed_model.dart';
import 'package:catbreedapp/ui/pages/detail_cat_breed_page.dart';

import '../atoms/button_atom.dart';
import 'cat_breed_card_molecule.dart';

class BreedButtonCardMolecule extends StatelessWidget {
  final CatBreed catBreed;
  const BreedButtonCardMolecule({Key? key, required this.catBreed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ButtonAtom(
        child: CatBreedCardMolecule(catBreed: catBreed),
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) => DetailCatBreedPage(catBreed: catBreed),
              ));
        });
  }
}
