import '../../domain/models/catbreed_model.dart';
import '../atoms/text_atom.dart';

class SearchCatBreedMolecule extends StatelessWidget {
  final List<CatBreed> catsBreeds;
  final Function(String value) funtion;
  const SearchCatBreedMolecule(
      {Key? key, required this.catsBreeds, required this.funtion})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Autocomplete<String>(
            optionsBuilder: (textEditingValue) {
              funtion(textEditingValue.text);
              return catsBreeds
                  .where((catBreed) => catBreed.name
                      .toLowerCase()
                      .contains(textEditingValue.text.toLowerCase()))
                  .map((e) => e.name);
            },
          ),
        ),
        const Icon(Icons.search)
      ],
    );
  }
}
