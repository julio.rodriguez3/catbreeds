import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import '../atoms/text_span_atom.dart';

class InformationItemMolecule extends StatelessWidget {
  final String name;
  final String? value;
  final bool isLink;
  final double? size;
  const InformationItemMolecule({
    Key? key,
    required this.name,
    required this.value,
    this.isLink = false,
    this.size,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(2.0),
      child: DefaultTextStyle(
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: size ?? 15),
          child: SelectableText.rich(TextSpanAtom(
              style: TextStyle(
                overflow: TextOverflow.clip,
                color: Theme.of(context).textTheme.subtitle1!.color,
              ),
              children: [
                TextSpanAtom(
                  style: const TextStyle(fontWeight: FontWeight.bold),
                  text: '$name: ',
                ),
                TextSpanAtom(
                  text: '$value',
                  style: TextStyle(color: isLink ? Colors.blue : null),
                  recognizer:
                      isLink ? (TapGestureRecognizer()..onTap = () {}) : null,
                )
              ]))),
    );
  }
}
