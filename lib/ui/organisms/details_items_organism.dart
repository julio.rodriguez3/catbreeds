import 'package:catbreedapp/domain/models/catbreed_model.dart';
import 'package:flutter/material.dart';

import '../molecules/information_item_molecule.dart';

class DetailsItemOrganism extends StatelessWidget {
  final CatBreed catData;
  final double sizeText;
  const DetailsItemOrganism(
      {Key? key, required this.catData, required this.sizeText})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        InformationItemMolecule(
            size: sizeText,
            name: 'Life expectancy (Years)',
            value: catData.lifeSpan),
        InformationItemMolecule(
          size: sizeText,
          name: 'Origin',
          value: catData.origin,
        ),
        InformationItemMolecule(
          size: sizeText,
          name: 'Intelligence',
          value: catData.intelligence.toString(),
        ),
        InformationItemMolecule(
          size: sizeText,
          name: 'Alt. Names',
          value: (catData.altNames == null)
              ? 'N/A'
              : (catData.altNames!.isEmpty ? 'N/A' : catData.altNames),
        ),
        InformationItemMolecule(
          size: sizeText,
          name: 'Temperament',
          value: catData.temperament,
        ),
        InformationItemMolecule(
          size: sizeText,
          name: 'Adaptability',
          value: catData.adaptability.toString(),
        ),
        InformationItemMolecule(
          size: sizeText,
          name: 'More datas',
          value: catData.wikipediaUrl,
          isLink: true,
        ),
      ],
    );
  }
}
