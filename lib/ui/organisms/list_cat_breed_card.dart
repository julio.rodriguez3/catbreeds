import 'package:catbreedapp/ui/responsive.dart';

import '../molecules/cat_breed_card_molecule.dart';

class ListCatBreedCardOrganism extends StatelessWidget {
  final List<Widget> catsBreed;
  const ListCatBreedCardOrganism({Key? key, required this.catsBreed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final isMovil=MediaQuery.of(context).isMobil;
    return isMovil? _ListCatBreedCardMobil(catsBreed: catsBreed):_ListCatBreedCardDesktop(catsBreed: catsBreed);
  }
}

class _ListCatBreedCardMobil extends StatelessWidget {
  final List<Widget> catsBreed;
  const _ListCatBreedCardMobil({Key? key, required this.catsBreed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: catsBreed,
    );
  }
}

class _ListCatBreedCardDesktop extends StatelessWidget {
  final List<Widget> catsBreed;
  const _ListCatBreedCardDesktop({Key? key, required this.catsBreed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Wrap(
alignment: WrapAlignment.center,
        children: catsBreed,
      ),
    );
  }
}