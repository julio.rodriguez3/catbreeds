import 'dart:async';

import '../domain/models/catbreed_model.dart';
import '../infraestructure/catbreed_api/catbreeds_api.dart';

class _CatBreedsBloc {
  // This stream has the responsability of listener the update cat data. For fetchAllBreed and filterBreed.
  // Singleton.
  final List<CatBreed> catsBreed = [];

  _CatBreedsBloc._private();

  static final _CatBreedsBloc _singleton = _CatBreedsBloc._private();
  factory _CatBreedsBloc() {
    return _singleton;
  }
  final CatBreedsApi _catBreedsApi = CatBreedsApi();

  ///

  final _breedStreamController = StreamController<List<CatBreed>>();

  StreamController<List<CatBreed>> get streamData {
    _fetchAllBreeds();
    return _breedStreamController;
  }

  _fetchAllBreeds() {
    _catBreedsApi.getCatBreed().then((value) {
      catsBreed.addAll(value);
      _breedStreamController.add(value);
    });
  }

  filterCatsBreedForName(String nameCatBreedToSearch) {
    _breedStreamController.add(catsBreed
        .where((catBreed) => catBreed.name
            .toLowerCase()
            .contains(nameCatBreedToSearch.toLowerCase()))
        .toList());
  }
}

// Only this instance is public to always handle the class.
final catbreedsBloc = _CatBreedsBloc();
