import 'dart:convert';

import 'package:catbreedapp/infraestructure/catbreed_api/errors/catbreeds_api_error.dart';
import 'package:http/http.dart' as http;

import 'package:catbreedapp/domain/models/catbreed_model.dart';
import 'package:catbreedapp/domain/models/gateway/catbreed_gateway.dart';

class CatBreedsApi extends CatBreedsGateway {
  final _apiKey = {
    'x-api-key': 'bda53789-d59e-46cd-9bc4-2936630fde39',
  };
  final Uri _url = Uri.https("api.thecatapi.com", "/v1/breeds");

  @override
  Future<List<CatBreed>> getCatBreed() async {
    final response = await http.get(
      _url, /* headers: _apiKey*/
    );
    if (response.statusCode != 200) {
      throw CatBreedsApiError();
    }
    List<CatBreed> catBreeds = [];
    json.decode(response.body).forEach((element) {
      catBreeds.add(CatBreed.fromMap(element));
    });
    return catBreeds;
  }
}
