# CatBreedApp

En esta App se consume una API que brinda información sobre razas de gatos. Se implemento Clean Code y Atomic Design.

## Getting Started

En Mobil la App puede ser simulada con normalidad.

Por otra parte, para simularla en Web, es necesario tener en cuenta el siguiente comando: flutter run -d chrome --web-renderer html esto debido al tipo de renderizado que utiliza por defecto Flutter Web. Para el renderizado con 'HTML' la app funciona bien, para el renderizado con 'CanvasKit', se presentan problemas en la visualización de las imágenes. Para más de detalles, se puede consultar el siguiente link en StackOverflow: https://stackoverflow.com/questions/66060984/flutter-web-image-loading-in-mobile-view-but-not-in-full-view.


